import React, { useState }  from 'react';
import { Button, Table, Form, Navbar } from 'react-bootstrap';
import Select from 'react-select';

import itunesGateway from './gateways/itunes.js'

import './App.css';

function App() {
  /* Define Local states using react hooks*/
  let [results, setResults] = useState(null);
  const [mediaType] = useState([
    { value: 'movie', label: 'Movie' },
    { value: 'podcast', label: 'Podcast' },
    { value: 'music', label: 'Music' },
    { value: 'musicVideo', label: 'Music Video' },
    { value: 'audiobook', label: 'Audiobook' },
    { value: 'shortFilm', label: 'Short Film' },
    { value: 'software', label: 'Software' },
    { value: 'tvShow', label: 'Tv Show' },
    { value: 'all', label: 'All' },
  ]);
  const [search, setSearch] = useState({
    media: '',
    type: mediaType[0],
  });
  const [ loading, setLoading] = useState(false);

  {/* Handle submit and get data from itunes */}
  const handleSearch = (e) => {
    e.preventDefault();
    setLoading(true);
    setResults(null);
    itunesGateway.getItunesMedia(search)
    .then((searchResults) => {
      setResults(searchResults.results)
      setLoading(false);
    })
  }

  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
      <div className="container">
        <Navbar.Brand href="#home">Pocket-buildings</Navbar.Brand>
      </div>
      </Navbar>
      <div className="container">
        <div className="media-wrapper">
          <div className="search-wrapper">
            <Form onSubmit={handleSearch}>
              <div className="row">
                  <div className="col-md-3">
                    <div className="elem-space-around">
                      <Select
                        name="media-type"
                        value={search.type}
                        options={mediaType}
                        onChange={(value) => {
                          setSearch({
                            media: search.media,
                            type: value
                          })
                        }}
                        defaultValue={search.type}
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="elem-space-around">
                      <Form.Control
                        type="text"
                        placeholder="Search media..."
                        value={search.media}
                        onChange={(event) => {
                          !event.target.value ? setResults(null) : setResults(results);
                          setSearch({
                            media: event.target.value,
                            type: search.type
                          });
                        }}
                        name="media"
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="text-left elem-space-around">
                      <Button type="submit" variant="outline-dark">Search</Button>
                    </div>
                  </div>
              </div>
            </Form> {/* Form for search */}
          </div>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Thumbnail</th>
                <th>Link</th>
              </tr>
            </thead>
            <tbody>
            {
              results && search.media && results.length ?
              results.map((row, index) => {
                return (
                  <tr key={index + Math.random()}>
                    <td><img src={row.artworkUrl100} alt="Itunes Thumbnail" /></td>
                    <td>{row.trackViewUrl}</td>
                  </tr>
                );
              }) : undefined
            }
            {
              !results && !loading ?
                <tr>
                  <td colSpan="2">
                    <p>
                      Search media...
                    </p>
                  </td>
                </tr>
                : undefined
            }
            {
              (results && !results.length) && !loading ?
                <tr>
                  <td colSpan="2">
                    <p>
                      No results found
                    </p>
                  </td>
                </tr>
                : undefined
            }
            {
              loading ?
                <tr>
                  <td colSpan="2">
                    <p>
                      Loading...
                    </p>
                  </td>
                </tr>
                : undefined
            }
            </tbody>
          </Table> {/* Display thumbnail and link in tabular format*/}
        </div>
      </div>
    </div>
  );
}

export default App;
