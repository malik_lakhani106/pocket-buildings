import axios from 'axios';

export default {
	getItunesMedia(search) {
		return axios.get('https://itunes.apple.com/search', {
      params: {
      	media: search.type.value,
        term: search.media
	    }
	  })
	  .then(function (response) {
      return response.data
	  })
	  .catch(function (error) {
	    console.log(error);
  	})
	}
}