This project is a test project to get some media details from itunes and show thumbnail and links. It also provides a couple of search filters for.

## Prerequisites:
* [Node.js >= 8.16.x/NPM](http://nodejs.org/download/)

## Setup:

* Clone the project
    ```
    git clone
    ```

* Install dependency
    ```
    npm install
    ```
* Run project:
    ```
    npm start
    ```
* To build the project:
    ```
    npm run build
    ```